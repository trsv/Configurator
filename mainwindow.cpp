#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "struct.h"
#include "dataio.h"
#include "tableitem.h"

#include <QMap>
#include <QMultiMap>
#include <QMessageBox>
#include <QDebug>
#include <QFileDialog>
#include <QComboBox>
#include <QTableWidget>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    resetViewModel();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_buttonGo_clicked()
{
    QList<Component> components;

    buildComponentList(components);

    if (components.size() == 0) {
        error("Конфигурация пуста");
        return;
    }

    Build build;
    build.m_components = components;
    build.m_row_name = ui->spinName->value();
    build.m_row_desc = ui->spinDesc->value();
    build.m_row_image = ui->spinImage->value();

    emit dataPrepared(build);
}

void MainWindow::loadData(const Setting & setting)
{
    if (setting.m_path_to_xlsx.isEmpty()) {
        error("Не указан путь к xlsx-файлу с данными");
        return;
    }

    m_list = m_io.readData(setting.m_path_to_xlsx);

    showData();
}

void MainWindow::showSetting(const Setting & setting)
{
    ui->linePathXlsx->setText(setting.m_path_to_xlsx);
    ui->linePathHtml->setText(setting.m_path_to_html);
    ui->linePathXlsxReport->setText(setting.m_path_to_xlsx_report);

    ui->lineReportName->setText(setting.m_report_name);
    ui->checkReportMultiply->setChecked(setting.m_report_multiply);

    ui->lineVarCpu->setText(setting.m_report_var_cpu);
    ui->lineVarHtmlDesc->setText(setting.m_report_var_html_desc);
    ui->lineVarPrice->setText(setting.m_report_var_price);

    ui->lineCategoryImage->setText(setting.m_image_category);
}

void MainWindow::saveSetting()
{
    Setting setting;

    setting.m_path_to_xlsx = ui->linePathXlsx->text();
    setting.m_path_to_html = ui->linePathHtml->text();
    setting.m_path_to_xlsx_report = ui->linePathXlsxReport->text();

    setting.m_report_name = ui->lineReportName->text();
    setting.m_report_multiply = ui->checkReportMultiply->isChecked();
    setting.m_report_var_cpu = ui->lineVarCpu->text();
    setting.m_report_var_html_desc = ui->lineVarHtmlDesc->text();
    setting.m_report_var_price = ui->lineVarPrice->text();

    setting.m_image_category = ui->lineCategoryImage->text();

    m_io.saveSetting(setting);
}

void MainWindow::error(const QString & message)
{
    QMessageBox::warning(
                this,
                tr("Конфигуратор"),
                tr(message.toStdString().c_str())
                );
}

void MainWindow::info(const QString &message)
{
    QMessageBox::information(
                this,
                tr("Конфигуратор"),
                tr(message.toStdString().c_str())
                );
}

void MainWindow::on_pushButton_clicked()
{
    QString empty_path = checkAllPath();
    if (!empty_path.isEmpty()) {
        error("Вы не указали путь к:" + empty_path);
        return;
    }

    saveSetting();
}

QString MainWindow::checkAllPath()
{
    QString empty;

    if (ui->linePathXlsx->text().isEmpty()) empty += "\nxlsx";
    if (ui->linePathHtml->text().isEmpty()) empty += "\nhtml";
    if (ui->linePathXlsxReport->text().isEmpty()) empty += "\nxlsx отчет";

    return empty;
}

void MainWindow::addComponentToView(const QString & category)
{
    int row = ui->tableView->rowCount();
    ui->tableView->insertRow(row);

    QComboBox * combo = new QComboBox(this);
    QStringList values;
    for (Component & c : m_list.values(category)) {
        values << c.m_display_name;
    }

    combo->addItems(values);

    QTableWidgetItem * item = new TableItem();
    item->setData(Qt::EditRole, category);
    ui->tableView->setItem(row, 0, item);

    ui->tableView->setCellWidget(row, 1, combo);

    ui->tableView->setItem(row, 3, new QTableWidgetItem("1"));

    Component component = getComponentByName(combo->currentText());

    ui->tableView->setItem(row, 2, new QTableWidgetItem(QString::number(component.m_price)));

    connect(combo, &QComboBox::currentTextChanged, this, &MainWindow::onComboBoxTextChanged);

    ui->tableView->sortByColumn(0);
}

void MainWindow::resetViewModel()
{
    ui->tableView->setColumnCount(4);

    QList <QString> headers;
    headers << "Тип" << "Название" << "Цена" << "Кол-во";
    ui->tableView->setHorizontalHeaderLabels(headers);

    ui->tableView->setColumnWidth(2, 100);
    ui->tableView->setColumnWidth(3, 60);
    ui->tableView->setColumnWidth(4, 95);

    ui->tableView->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Stretch);
    ui->tableView->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Stretch);

    ui->tableView->setSortingEnabled(true);
}

void MainWindow::buildComponentList(QList<Component> &list)
{
    list.clear();

    for (int row = 0; row < ui->tableView->rowCount(); row++) {

        QString compomemt_name = ((QComboBox*)ui->tableView->cellWidget(row, 1))->currentText();

        Component component = getComponentByName(compomemt_name);

        component.m_count = getValueInt(row, 3);

        list.append(component);
    }
}

Component MainWindow::getComponentByName(const QString &name)
{
    QList <Component> values = m_list.values();

    for (Component & component : values) {
        if (component.m_display_name == name) return component;
    }

    return m_list.end().value();
}

void MainWindow::changePriceOnView()
{
    double price = 0.0;

    for (int row = 0; row < ui->tableView->rowCount(); row++) {

        int count = 0;
        if (ui->tableView->item(row, 3) != NULL) count = ui->tableView->item(row, 3)->text().toInt();


        price += count * ui->tableView->item(row, 2)->text().toDouble();
    }

    qDebug() << "total price:" << price;
    ui->lineTotal->setText(QString::number(price));
}

void MainWindow::onComboBoxTextChanged(const QString &text)
{
    qDebug() << text;

    int row = 0;

    for (; row < ui->tableView->rowCount(); row++) {
        QString current_text = ((QComboBox *)ui->tableView->cellWidget(row, 1))->currentText();

        if (current_text == text) break;
    }

    Component c = getComponentByName(text);

    ui->tableView->setItem(row, 2, new QTableWidgetItem(QString::number(c.m_price)));

    changePriceOnView();

}

void MainWindow::onCellPriceChanged(int row, int column)
{
    if (column == 2 || column == 3) changePriceOnView();
}

void MainWindow::showData()
{
    QSet <QString> categories = m_list.keys().toSet();

    for (const QString & category : categories) {
        addComponentToView(category);
    }

    connect(ui->tableView, &QTableWidget::cellChanged, this, &MainWindow::onCellPriceChanged);

    changePriceOnView();
}

void MainWindow::showHtml(QList<QString> &lines)
{
    QString html;
    for (QString & string : lines) html += string + "\n";

    ui->viewHtml->setPlainText(html);
}

bool MainWindow::isAllHtml()
{
    return ui->checkHtmlAll->isChecked();
}

int MainWindow::getValueInt(int row, int column)
{
    if (ui->tableView->item(row, column) == NULL) return 0;
    else return ui->tableView->item(row, column)->text().toInt();
}

void MainWindow::on_toolPathXlsx_clicked()
{
    QString path_open = QDir::currentPath();

    QString path = QFileDialog::getOpenFileName(this, tr("Выберите MS Excel файл"), path_open, "MS Excel (*.xls *.xlsx)");

    if (!path.isEmpty()) ui->linePathXlsx->setText(path);
}

void MainWindow::on_toolPathHtml_clicked()
{
    QString path_open = QDir::currentPath();

    QString path = QFileDialog::getOpenFileName(this, tr("Выберите Html файл"), path_open, "HTML (*.htm *.html)");

    if (!path.isEmpty()) ui->linePathHtml->setText(path);
}

void MainWindow::on_checkHtmlAll_clicked()
{
    emit showHtmlAll();
}

void MainWindow::on_checkHtmlName_clicked()
{
    emit showHtmlShort();
}

void MainWindow::on_toolPathXlsxReport_clicked()
{
    QString path_open = QDir::currentPath();

    QString path = QFileDialog::getOpenFileName(this, tr("Выберите MS Excel файл"), path_open, "MS Excel (*.xls *.xlsx)");

    if (!path.isEmpty()) ui->linePathXlsxReport->setText(path);
}
