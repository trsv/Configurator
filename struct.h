#ifndef STRUCT_H
#define STRUCT_H

#include <QString>
#include <QStringList>
#include <QMap>

// данные разметки о компоненте
struct Html
{
    // разметка
    QString m_name;
    QString m_desc;
    QString m_image;
};

// структура компонента в xlsx файле
struct Component
{
    QString m_category;     // категория (cpu, gpu, ram)
    QString m_display_name; // название (Asus, MSI)

    int m_count;            // количество компонентов
    double m_price;         // цена
    QString m_value;        // значение (частота процессора, память озу и т.д.)

    Html m_html;            // html данные о компоненте
};

// структура сборки компьютора
struct Build
{
    QList <Component> m_components;

    int m_row_name = 0;
    int m_row_desc = 0;
    int m_row_image = 0;

    QMap <QString, Component> toMap() {
        QMap <QString, Component> map;
        for (Component & component : m_components) map.insert(component.m_category, component);
        return map;
    }
};

struct Setting {
    QString m_path_to_xlsx;
    QString m_path_to_html;
    QString m_path_to_xlsx_report;

    QString m_report_name;
    QString m_report_var_cpu;
    QString m_report_var_html_desc;
    QString m_report_var_price;

    bool m_report_multiply;

    QString m_image_category;
};

struct Report
{
    QMap <QString, Component> m_list;
    double m_price_total;

    QString m_var_cpu;
    QString m_var_html_desc;
    QString m_var_price;

    QString m_html_desc;

    void prepare() {
        m_price_total = 0.0;

        for (Component & component : m_list) {
            m_price_total += component.m_price * component.m_count;
        }
    }
};

struct Template {
    QString m_key;
    QString m_value;
    int m_column;
};

#endif // STRUCT_H
