#include "dataio.h"
#include "xlsxdocument.h"

#include <QFile>
#include <QSettings>
#include <QDebug>
#include <QDateTime>

DataIO::DataIO()
{

}

QMultiMap<QString, Component> DataIO::readData(const QString & path)
{
    QMultiMap <QString, Component> list;

    QXlsx::Document document(path);

    int lastRow = document.dimension().lastRow();
    QString category, value;

    for (int row = 1; row <= lastRow; row++) {

        Component component;

        if (QXlsx::Cell * cell = document.cellAt(row, 1)) value = cell->value().toString();
        if (!value.isEmpty()) category = value;

        component.m_category = category.trimmed();
        if (QXlsx::Cell * cell = document.cellAt(row, 2)) component.m_display_name = cell->value().toString().trimmed();
        if (QXlsx::Cell * cell = document.cellAt(row, 3)) component.m_value = cell->value().toString().trimmed();
        if (QXlsx::Cell * cell = document.cellAt(row, 4)) component.m_price = cell->value().toDouble();

        if (QXlsx::Cell * cell = document.cellAt(row, 5)) component.m_html.m_name = cell->value().toString().trimmed();
        if (QXlsx::Cell * cell = document.cellAt(row, 6)) component.m_html.m_desc = cell->value().toString().trimmed();
        if (QXlsx::Cell * cell = document.cellAt(row, 7)) component.m_html.m_image = cell->value().toString().trimmed();

        list.insert(component.m_category, component);
    }

    return list;
}

QString DataIO::readHtml(const QString & path)
{
    QFile file(path.toStdString().c_str());

    file.open(QIODevice::ReadOnly | QIODevice::Text);

    QString html = file.readAll();

    file.close();

    return html;
}

void DataIO::saveHTml(QList <QString> & html, const QString & path)
{
    QFile file(path);

    if (!file.open(QIODevice::Text | QIODevice::WriteOnly)) {
        qDebug() << "[ERROR] Can`t save file" << path;
        return;
    }

    for (QString & str : html) {
        str.append("\n");
        file.write(str.toStdString().c_str());
    }

    file.close();
}

void DataIO::saveReport(Report & report, const Setting & setting)
{
    QList <Template> data = readStruct(setting.m_path_to_xlsx_report);

    QString path = setting.m_report_name;
    bool multiply = setting.m_report_multiply;

    QXlsx::Document document(path);

    bool needHeader = !QFile(path).exists();

    int row;
    if (multiply && !needHeader) row = document.dimension().lastRow() + 1;
    else row = 2;

    if (needHeader) {

        int column = 1;

        for (Template & t : data) {
            document.write(1, column, t.m_key);
            column++;
        }
    }

    int column = 1;
    bool skipCpu = false;
    for (Template & t : data) {

        if (skipCpu) {
            skipCpu = false;
            continue;
        }

        if (!t.m_value.isEmpty()) {
            document.write(row, t.m_column, t.m_value);
            column++;
            continue;
        }

        QString header = t.m_key;

        if (header == report.m_var_html_desc) {
            document.write(row, column, report.m_html_desc);
            column++;
            continue;
        }

        if (header == report.m_var_price) {
            document.write(row, column, report.m_price_total);
            column++;
            continue;
        }

        Component component = report.m_list.value(header, Component());

        if (header == report.m_var_cpu) {
            document.write(row, column, component.m_display_name);
            column++;
            document.write(row, column, component.m_value);
            skipCpu = true;
        } else {
            document.write(row, column, component.m_display_name);
        }

        column++;
    }

    if (multiply) {
        document.save();
    } else {
        QString id = QDateTime::currentDateTime().toString("-yyyy-MM-dd_HH-mm-ss");
        QString new_path = path;
        new_path = new_path.insert(new_path.lastIndexOf("."), id);
        document.saveAs(new_path);
    }
}

Setting DataIO::readSetting()
{
    QSettings config("config.ini", QSettings::IniFormat);

    Setting setting;

    setting.m_path_to_xlsx = config.value("path/path_to_xlsx", "").toString();
    setting.m_path_to_html = config.value("path/path_to_html", "").toString();
    setting.m_path_to_xlsx_report = config.value("path/path_to_xlsx_report", "").toString();

    setting.m_report_name = config.value("report/name", "report.xlsx").toString();
    setting.m_report_multiply = config.value("report/multiply").toBool();

    setting.m_report_var_cpu = config.value("report/var_cpu", "").toString();
    setting.m_report_var_html_desc = config.value("report/var_html_desc").toString();
    setting.m_report_var_price = config.value("report/var_price", "").toString();

    setting.m_image_category = config.value("html/category_image").toString();

    return setting;
}

void DataIO::saveSetting(Setting & setting)
{
    QSettings config("config.ini", QSettings::IniFormat);

    config.setValue("path/path_to_xlsx", setting.m_path_to_xlsx);
    config.setValue("path/path_to_html", setting.m_path_to_html);
    config.setValue("path/path_to_xlsx_report",  setting.m_path_to_xlsx_report);

    config.setValue("report/name", setting.m_report_name);
    config.setValue("report/multiply", setting.m_report_multiply);

    config.setValue("report/var_cpu", setting.m_report_var_cpu);
    config.setValue("report/var_html_desc", setting.m_report_var_html_desc);
    config.setValue("report/var_price", setting.m_report_var_price);

    config.setValue("html/category_image", setting.m_image_category);
}

QList<Template> DataIO::readStruct(const QString &path)
{
    QList <Template> data;

    QXlsx::Document document(path);

    qDebug() << "last column:" << document.dimension().lastColumn();

    for (int column = 1; column <= document.dimension().lastColumn(); column++) {

        Template t;

        QString key;
        if (QXlsx::Cell * cell = document.cellAt(1, column)) key = cell->value().toString();

        QString value;
        if (QXlsx::Cell * cell = document.cellAt(2, column)) value = cell->value().toString();

        t.m_key = key;
        t.m_value = value;
        t.m_column = column;

        data.append(t);
    }

    return data;
}

void DataIO::WriteHtml(QList <QString> &html, Build & build, Setting & setting)
{
    int shift = 0;

    if (build.m_row_image > 0) {
        for (Component & component : build.m_components) {
            if (component.m_category == setting.m_image_category) {
                html.insert(build.m_row_image - 1, component.m_html.m_image);
                shift++;
                break;
            }
        }
    }

    if (build.m_row_name > 0) {

        QString html_name;

        for (Component & component : build.m_components) {
            html_name += component.m_html.m_name + "\n\n";
        }

        html.insert(build.m_row_name + shift - 1, html_name);
        shift++;
    }

    if (build.m_row_desc > 0) {

        QString html_desc;

        for (Component & component : build.m_components) {
            html_desc += component.m_html.m_desc + "\n\n";
        }

        html.insert(build.m_row_desc + shift - 1, html_desc);
    }
}
