#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "dataio.h"

#include <memory>

#include <QMainWindow>
#include <QMultiMap>
#include <QStandardItemModel>
#include <QSortFilterProxyModel>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

signals:
    void dataPrepared(Build & build);

    void showHtmlShort();
    void showHtmlAll();

public slots:
    void loadData(const Setting & setting);

    void showSetting(const Setting & setting);
    void saveSetting();

    void error(const QString & message);
    void info(const QString & message);

    void addComponentToView(const QString &category);

    void showHtml(QList <QString> & lines);

    bool isAllHtml();

private slots:
    void on_buttonGo_clicked();
    void on_pushButton_clicked();
    void on_toolPathXlsx_clicked();
    void on_toolPathHtml_clicked();

    QString checkAllPath();

    void resetViewModel();

    void buildComponentList(QList <Component> & list);
    Component getComponentByName(const QString & name);

    void changePriceOnView();

    void onComboBoxTextChanged(const QString & text);
    void onCellPriceChanged(int row, int column);

    void showData();

    int getValueInt(int row, int column);

    void on_checkHtmlAll_clicked();

    void on_checkHtmlName_clicked();

    void on_toolPathXlsxReport_clicked();

private:
    Ui::MainWindow *ui;

    std::unique_ptr <QStandardItemModel> m_model;
    std::unique_ptr <QSortFilterProxyModel> m_model_support;

    DataIO m_io;

    QMultiMap <QString, Component> m_list;
};

#endif // MAINWINDOW_H
