#ifndef PRESENTER_H
#define PRESENTER_H

#include "mainwindow.h"
#include "dataio.h"

#include <memory>

#include <QObject>

class Presenter : public QObject
{
    Q_OBJECT

public:
    explicit Presenter(QObject * parent = 0);

private slots:
    void process(Build & build);
    void initSetting();

    void showMainWindow();

    void showHtmlShort();
    void showHtmlAll();

    QList<QString> buildHtmlShort(QList<Component> & components);

private:
    std::unique_ptr <MainWindow> m_window_main;

    DataIO m_io;
    Setting m_setting;

    QList <QString> m_html_all;
    QList <QString> m_html_short;
};

#endif // PRESENTER_H
