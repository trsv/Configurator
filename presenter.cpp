#include "presenter.h"

#include <QDebug>

Presenter::Presenter(QObject *parent) : QObject(parent)
{
    showMainWindow();

    initSetting();

    QObject::connect(m_window_main.get(), &MainWindow::dataPrepared, this, &Presenter::process);

    QObject::connect(m_window_main.get(), &MainWindow::showHtmlAll, this, &Presenter::showHtmlAll);
    QObject::connect(m_window_main.get(), &MainWindow::showHtmlShort, this, &Presenter::showHtmlShort);
}

void Presenter::process(Build & build)
{
    m_html_all = m_io.readHtml(m_setting.m_path_to_html).split("\n");
    m_html_short = buildHtmlShort(build.m_components);

    m_io.WriteHtml(m_html_all, build, m_setting);

    Report report;
    report.m_list = build.toMap();

    report.m_html_desc.clear();
    for (QString & string : m_html_all) report.m_html_desc += string + "\n";

    report.m_var_cpu = m_setting.m_report_var_cpu;
    report.m_var_html_desc = m_setting.m_report_var_html_desc;
    report.m_var_price = m_setting.m_report_var_price;
    report.prepare();

    m_io.saveReport(report, m_setting);

    if (m_window_main->isAllHtml()) showHtmlAll();
    else showHtmlShort();
}

void Presenter::initSetting()
{
    m_setting = m_io.readSetting();

    m_window_main->loadData(m_setting);
    m_window_main->showSetting(m_setting);
}

void Presenter::showMainWindow()
{
    m_window_main.reset(new MainWindow);
    m_window_main->show();
}

void Presenter::showHtmlShort()
{
    m_window_main->showHtml(m_html_short);
}

void Presenter::showHtmlAll()
{
    m_window_main->showHtml(m_html_all);
}

QList <QString> Presenter::buildHtmlShort(QList<Component> & components)
{
    QList <QString> html;

    for (Component & component : components) html << component.m_html.m_name;

    return html;
}
