#ifndef DATAIO_H
#define DATAIO_H

#include "struct.h"

#include <QMultiMap>

class DataIO
{
public:
    DataIO();

public:
    QMultiMap <QString, Component> readData(const QString & path);

    QString readHtml(const QString &);
    void saveHTml(QList <QString> & html, const QString & path);

    void saveReport(Report & report, const Setting &setting);

    Setting readSetting();
    void saveSetting(Setting & setting);

    QList<Template> readStruct(const QString & path);

    void WriteHtml(QList<QString> &list,  Build &build, Setting &setting);
private:

};

#endif // DATAIO_H
