#include "presenter.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    std::unique_ptr <Presenter> m_presenter;
    m_presenter.reset(new Presenter);

    return a.exec();
}
