#ifndef TABLEITEM_H
#define TABLEITEM_H

#include <QTableWidgetItem>
#include <QRegularExpression>

class TableItem : public QTableWidgetItem
{
public:
    TableItem() {
        m_regex.setPattern("(\\d+)");
    }

    bool operator < (const QTableWidgetItem &other) const {

        int current = m_regex.match(this->text()).captured(1).toInt();
        int next = m_regex.match(other.text()).captured(1).toInt();

        return current > next;
    }

private:
    QRegularExpression m_regex;
};

#endif // TABLEITEM_H
